//
//  Note.swift
//  NoteApp2
//
//  Created by -全 on 2018/6/10.
//  Copyright © 2018年 -全. All rights reserved.
//

import UIKit

class Note: NSObject {
    var text = String()
    //var image = UIImage()
    var imageName : String?
    func image() -> UIImage?{
        if let fileName = self.imageName {
        let homeURL = NSURL(fileURLWithPath: NSHomeDirectory())
        let documentURL = homeURL.appendingPathComponent("Documents")
        let fileURL = documentURL?.appendingPathComponent(fileName)
            if let imageData = NSData(contentsOf: fileURL!) {
                return UIImage(data: imageData as Data)
            }
        }
        return nil
    }
    
}
