//
//  NoteListViewController.swift
//  NoteApp2
//
//  Created by -全 on 2018/6/10.
//  Copyright © 2018年 -全. All rights reserved.
//

import UIKit

class NoteListViewController: UIViewController,UITableViewDataSource,NoteViewControllerDelegate {
    @IBOutlet weak var tableView: UITableView!
    var notes : [Note] = []
    required init?(coder aDecoder: NSCoder) {
        for index in 0..<10 {
            let note = Note()
            note.text = "Note \(index)"
            self.notes.append(note)
        }
        
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self

        self.navigationItem.leftBarButtonItem = self.editButtonItem
    }
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func addNote(_ sender: Any) {
        let note = Note()
        note.text = "New Note"
        self.notes.append(note) // 新增在最後
        //self.notes.insert(note, at: 0)  //新增在第一筆
        let indexPath = NSIndexPath(row: self.notes.count-1, section: 0)
        
        self.tableView.insertRows(at: [indexPath as IndexPath], with: .automatic)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notes.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell", for: indexPath)
        let note = self.notes[indexPath.row]
        cell.textLabel?.text = note.text
        cell.imageView?.image = note.image()
        cell.showsReorderControl = true
        
        return cell
    }
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let note = self.notes[sourceIndexPath.row]
        self.notes.remove(at: sourceIndexPath.row)
        self.notes.insert(note, at: destinationIndexPath.row)
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if ( editingStyle == .delete){
            self.notes.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "noteViewController" {
            let noteViewController = segue.destination as! NoteViewController
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let note = self.notes[indexPath.row]
                noteViewController.note = note
                noteViewController.delegate = self
            }
        }
    }
    func didFinishUpdateNote(note: Note) {
        if let index = self.notes.index(of: note) {
            let indexPath = NSIndexPath(row: index, section: 0)
            self.tableView.reloadRows(at: [indexPath as IndexPath], with: .automatic)
        }
        
    }
 

}
